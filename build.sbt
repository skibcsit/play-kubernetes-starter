scalaVersion := "2.13.10"

// renovate: datasource=repology depName=debian_unstable/openjdk-11 extractVersion=^(?<version>[\d\.]+)(\+[\w\d]+)?$ versioning=loose
val javaVersion = "11.0.16"

lazy val root = (project in file(".")).enablePlugins(PlayScala, DockerPlugin)

dockerBaseImage := s"openjdk:$javaVersion-jre"
dockerExposedPorts := Seq(9000)
scalafmtOnCompile := !insideCI.value

libraryDependencies ++= Seq(
  "com.softwaremill.macwire" %% "macros" % "2.5.9" % "provided",
)

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

import com.typesafe.sbt.packager.docker.DockerChmodType

dockerChmodType := DockerChmodType.UserGroupWriteExecute
