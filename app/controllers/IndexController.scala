package controllers

import play.api.mvc.{AbstractController, ControllerComponents}

class IndexController(cc: ControllerComponents) extends AbstractController(cc) {
  def index = Action(implicit request => Ok("OK"))
  def healthz = Action(implicit request => Ok("alive"))
}
