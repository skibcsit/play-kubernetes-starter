package infrastructure

import com.softwaremill.macwire._
import controllers._
import play.api.ApplicationLoader.Context
import play.api._
import play.filters.csrf.CSRFComponents
import play.filters.headers.SecurityHeadersComponents
import play.filters.hosts.AllowedHostsComponents
import play.filters.https.RedirectHttpsComponents

final class MainApplicationLoader extends ApplicationLoader {
  def load(context: Context) = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
    new MainComponents(context).application
  }
}

//noinspection ScalaUnusedSymbol
final class MainComponents(context: Context)
    extends BuiltInComponentsFromContext(context)
    with CSRFComponents
    with SecurityHeadersComponents
    with AllowedHostsComponents
    with RedirectHttpsComponents
    with AssetsComponents
    with TimeZoneComponents {

  implicit private lazy val implicitAssetsFinder = assetsFinder

  lazy val config = configuration.underlying

  lazy val httpFilters = Seq(
    csrfFilter,
    securityHeadersFilter,
    redirectHttpsFilter,
  )

  private lazy val indexController = wire[IndexController]

  private lazy val routePrefix = "/"
  lazy val router = wire[_root_.router.Routes]

}
